## Links

This app was created as part of a project for a Software Engineering class at FER (https://www.fer.unizg.hr/predmet/proinz).

This is only the backend side of the webpage, deployed here -> https://skill-et-cooking-backend.herokuapp.com 

Our frontend project source code can be found here -> https://gitlab.com/vlak-u-stogu/skill-et-cooking-frontend

Our frontend is deployed here -> https://skill-et-cooking.herokuapp.com

More information about the app (such as the database definition, or open endpoints) can be found in the wiki (https://gitlab.com/vlak-u-stogu/skill-et-cooking-backend/-/wikis/home) or in official documentation (PROGI_2021_VlakUStogu_v1_0.pdf).


## Installation guide

If you want to download and run this backend locally, follow these steps:

1. Make sure you have Maven installed.
2. Setup a local version of our PostgreSql database. SQL commands used to generate an empty version of our database can be found in our wiki (https://gitlab.com/vlak-u-stogu/skill-et-cooking-backend/-/wikis/Database).  
3. This app requires an image service for storing images. For this, create an account on https://imgbb.com, and find your account's API key. Set an "image_api_key" environmental variable on your system to this API key.
4. This app also uses a "SECRET_KEY" environmental variable to hash passwords, so set that to any string you want on your system.
5. `git clone` or just download as zip (and then unzip) the source files
6. Open `src/main/resources/application.properties`. Uncomment the first three lines (spring.datasource.url, spring.datasource.username, spring.datasource.password) and set those variables to appropriate values so Spring can connect to your local database. You can also edit the port that you want this app to use on the last line of this file (server.port) if your default HTTP port 80 is already in use.
7. `cd` to the downloaded folder, where the pom.xml file is located
8. `mvn spring-boot:run` will now automatically download all needed dependencies using Maven, compile the code and run the app. It will try to connect to your local database (make sure it is running and correct credentials are given) and expose HTTP endpoints on port 80.
9. App should be running - try opening `localhost` in your browser, it should now output a simple "hello world" string with the current app version attached.

Alternatively, you can load the downloaded project into your favourite IDE and run the `src/main/java/Application.java` file. This process may vary depending on what IDE you use.
