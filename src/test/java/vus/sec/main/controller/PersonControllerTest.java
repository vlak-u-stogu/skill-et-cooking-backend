package vus.sec.main.controller;

import org.mockito.Mockito;
import org.mockito.Spy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Base64;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mindrot.jbcrypt.BCrypt;

import vus.sec.main.person.Person;
import vus.sec.main.person.PersonService;
import vus.sec.main.recipe.RecipeService;

@WebMvcTest(PersonController.class)
@ExtendWith(SpringExtension.class)
@RunWith(SpringRunner.class)
public class PersonControllerTest {

	@Autowired
	private MockMvc mvc;
	
	private JSONParser parser = new JSONParser();
	private Base64.Decoder decoder = Base64.getUrlDecoder();
	
	@MockBean
	private PersonService personService;
	
	@MockBean
	private RecipeService recipeService;
	
	@Spy
	private Person person = Mockito.spy(new Person("user123", "user123@gmail.com", "hash"));
	
	@Test
	public void testRegisterSuccessful() throws Exception {
		
		when(personService.getUsersByEmail("user123@gmail.com")).thenReturn(null);
		when(personService.getUsersByUsername("user123")).thenReturn(null);
		
		mvc.perform(post("/user/register")
				.contentType("application/json")
				.content("{"
						+ "    \"username\": \"user123\","
						+ "    \"email\": \"user123@gmail.com\","
						+ "    \"password\": \"12345678\""
						+ "}")
				)
			.andExpect(status().isOk());
		
		// requires SECRET_KEY environment variable when running
		verify(personService).addUser(new Person("user123", "user123@gmail.com", 
				BCrypt.hashpw("12345678", System.getenv("SECRET_KEY")))); 
	}
	
	@Test
	public void testRegisterFailIfUsernameTooShort() throws Exception {
		mvc.perform(post("/user/register")
				.contentType("application/json")
				.content("{"
						+ "    \"username\": \"user\","
						+ "    \"email\": \"user123@gmail.com\","
						+ "    \"password\": \"12345678\""
						+ "}")
				)
			.andExpect(status().isForbidden());
	}
	
	@Test
	public void testRegisterFailIfUserExists() throws Exception {
		
		when(personService.getUsersByEmail("user123@gmail.com")).thenReturn(new Person("user123456", "user123@gmail.com", "hash"));
		when(personService.getUsersByUsername("user123")).thenReturn(null);
		
		mvc.perform(post("/user/register")
				.contentType("application/json")
				.content("{"
						+ "    \"username\": \"user123\","
						+ "    \"email\": \"user123@gmail.com\","
						+ "    \"password\": \"12345678\""
						+ "}")
				)
			.andExpect(status().isForbidden());
	}
	
	@Test
	public void testLoginSuccessful() throws Exception {
		
		when(person.checkPassword("12345678")).thenReturn(false);
		when(personService.getUsersByUsername("user123")).thenReturn(person);
		
		MvcResult result = mvc.perform(post("/user/login")
				.contentType("application/json")
				.content("{"
						+ "    \"usernameORemail\": \"user123\","
						+ "    \"password\": \"12345678\""
						+ "}")
				)
			.andExpect(status().isOk())
			.andReturn();		
		
		JSONObject json = (JSONObject) parser.parse(result.getResponse().getContentAsString());
		
		String jwt = (String) json.get("jwt");
		String[] chunks = jwt.split("\\.");
		JSONObject header = (JSONObject) parser.parse(new String(decoder.decode(chunks[0])));
		JSONObject payload = (JSONObject) parser.parse(new String(decoder.decode(chunks[1])));
		
		assertEquals("JWT", header.get("typ"));
		assertEquals("HS256", header.get("alg"));
		assertEquals("user123", payload.get("username"));
		
		String username = (String) json.get("username");
		long isModerator = (long) json.get("isModerator");
		
		assertEquals("user123", username);
		assertEquals(0, isModerator);
	}
	
	
	@Test
	public void testLoginFailed() throws Exception {
		when(person.checkPassword("wrongPassword")).thenReturn(true); // user typed the wrong password
		when(personService.getUsersByUsername("user123")).thenReturn(person);
		
		mvc.perform(post("/user/login")
				.contentType("application/json")
				.content("{"
						+ "    \"usernameORemail\": \"user123\","
						+ "    \"password\": \"wrongPassword\""
						+ "}")
				)
			.andExpect(status().isForbidden());		
	}
	
	
}
