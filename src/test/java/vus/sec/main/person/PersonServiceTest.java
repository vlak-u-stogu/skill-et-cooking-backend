package vus.sec.main.person;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest(classes = {PersonService.class})
@ExtendWith(SpringExtension.class)
@RunWith(SpringRunner.class)
public class PersonServiceTest {

	@Autowired
	private PersonService personService;
	
	@MockBean
	private PersonRepository personRepo;

	public static final Person PERSON = new Person("user123", "user123@gmail.com", "hash");
	
	@Test
	public void testGetByUsername() {
		when(personRepo.getPersonByUsername("user123")).thenReturn(PERSON);
		assertEquals(PERSON, personService.getUsersByUsername("user123"));
	}
	
	@Test
	public void testGetByEmail() {
		when(personRepo.getPersonByEmail("user123@gmail.com")).thenReturn(PERSON);
		assertEquals(PERSON, personService.getUsersByEmail("user123@gmail.com"));
	}
	
	@Test
	public void testGetById() {
		when(personRepo.findById(5)).thenReturn(Optional.of(PERSON));
		assertEquals(PERSON, personService.getUsersByID(5));
	}
}
