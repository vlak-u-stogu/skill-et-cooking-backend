package vus.sec.main.ingredient;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import java.util.Objects;

@Entity
public class Ingredient implements Comparable<Ingredient> {
    @Id
    @GeneratedValue(generator = "ingredient_ingredientid_seq")
    private int ingredientID;
    @NotBlank(message = "Ingredient must have a name.")
    private String name;
    public Ingredient(String name){
        this.name = name;
    }
    public Ingredient(){
    }

    public int getIngredientID() {
        return ingredientID;
    }

    public void setIngredientID(int ingredientID) {
        this.ingredientID = ingredientID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Ingredient that = (Ingredient) o;
        return ingredientID == that.ingredientID && name.equals(that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ingredientID, name);
    }
    @Override
    public int compareTo(Ingredient o){
        int r = this.ingredientID - o.ingredientID;
        if(r != 0) return r;
        else {
            return this.name.compareTo(o.name);
        }
    }
    @Override
    public String toString() {
        return "Ingredient{" +
                "ingredientID=" + ingredientID +
                ", name='" + name + '\'' +
                '}';
    }
}
