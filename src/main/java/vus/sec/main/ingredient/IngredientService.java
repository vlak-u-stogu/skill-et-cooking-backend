package vus.sec.main.ingredient;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class IngredientService {
    @Autowired
    private IngredientRepository ingredientRepository;

    public List<Ingredient> getIngredientsByName(int n, String name){
        List<Ingredient> ingredients;
        if(name != null) ingredients = ingredientRepository.findAll().stream()
                .filter(ingredient -> ingredient.getName().contains(name))
                .collect(Collectors.toList());
        else{
            ingredients = ingredientRepository.findAll();
        }
        if(n < ingredients.size())
            return ingredients.subList(0, n);
        else
            return ingredients;
    }
    public void addIngredient(Ingredient ingredient){
        ingredient.setName(ingredient.getName().toLowerCase());
        ingredientRepository.save(ingredient);
    }
    public boolean checkIfAlreadyAdded(Ingredient ingredient){
        // RETURNS true IF EXISTS
        if( ingredientRepository.getIngredientByName(ingredient.getName().toLowerCase()) != null) return true;
        return false;
    }
}
