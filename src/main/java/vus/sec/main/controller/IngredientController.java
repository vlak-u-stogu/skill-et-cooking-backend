package vus.sec.main.controller;

import com.auth0.jwt.exceptions.JWTDecodeException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import vus.sec.main.communication.JWTRequest;
import vus.sec.main.communication.UnauthorizedException;
import vus.sec.main.ingredient.Ingredient;
import vus.sec.main.ingredient.IngredientService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/ingredient")
public class IngredientController {
    @Autowired
    private IngredientService ingredientService;

    @CrossOrigin
    @GetMapping(path = "/")
    public List<Ingredient> getIngredientsByName(@RequestParam(value = "num") int num,
                                                 @RequestParam(value = "search", required = false) String search){
        return ingredientService.getIngredientsByName(num, search);
    }

    @CrossOrigin
    @PostMapping
    public Ingredient addIngredient(@Valid @RequestBody Ingredient ingredient,
                              @RequestParam("auth") String jwt) throws UnauthorizedException {
        try {
            JWTRequest jwtRequest = new JWTRequest(jwt);
            if(jwtRequest.isValid() && !jwtRequest.isMod()
                    && !ingredientService.checkIfAlreadyAdded(ingredient)){
                ingredientService.addIngredient(ingredient);
                return ingredient;
            } else throw new UnauthorizedException("Not authorized to do this.");
        }   catch (JWTDecodeException e){
            throw new UnauthorizedException("Not authorized to do this.");
        }


    }
}
