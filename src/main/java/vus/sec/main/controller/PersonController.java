package vus.sec.main.controller;

import com.auth0.jwt.exceptions.JWTDecodeException;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import vus.sec.main.communication.JWTRequest;
import vus.sec.main.communication.JWTResponse;
import vus.sec.main.communication.LoginForm;
import vus.sec.main.communication.RegisterForm;
import vus.sec.main.communication.UnauthorizedException;
import vus.sec.main.person.Person;
import vus.sec.main.person.PersonException;
import vus.sec.main.person.PersonService;
import vus.sec.main.recipe.Recipe;
import vus.sec.main.recipe.RecipeService;

import javax.validation.Valid;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/user")
public class PersonController {

    @Autowired
    private PersonService personService;
    @Autowired
    private RecipeService recipeService;
    
    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET)
    public Person getUsersByParams(@RequestParam(value = "auth") String jwt){
        try {
            JWTRequest request = new JWTRequest(jwt);

            if(request.isValid()){
                return personService.getUsersByUsername(request.getUsername());
            }
            else
                throw new UnauthorizedException("Not authorized for this content.");
            
        } catch (JWTDecodeException e){
            throw new UnauthorizedException("Not authorized for this content.");
        }

    }

    @CrossOrigin
    @RequestMapping(path = "/login", method = RequestMethod.POST)
    public JWTResponse login(@RequestBody LoginForm login) throws PersonException {
    	
        Person p = personService.getUsersByUsername(login.getUsernameORemail());
        if(p == null) {
            p = personService.getUsersByEmail(login.getUsernameORemail().toLowerCase());
            if(p == null) throw new PersonException("Incorrect username or password.");
        }

        if(p.checkPassword(login.getPassword())) {
        	throw new PersonException("Incorrect username or password.");
        }
            
        JWTResponse response = new JWTResponse(p.getUsername(), p.getIsModerator());
        return response;
    }
    
    @CrossOrigin
    @RequestMapping(path = "/register", method = RequestMethod.POST)
    public void register(@Valid @RequestBody RegisterForm register, Errors errors) throws PersonException {
    	
    	if (errors.hasErrors()) {
           AtomicReference<String> errMsg = new AtomicReference<>("");
           errors.getAllErrors()
                   .stream()
                   .forEach(objectError -> errMsg.set(errMsg + objectError.getDefaultMessage() + " "));
           throw new PersonException(errMsg.get());
        }
    	
    	if (personService.getUsersByEmail(register.getEmail()) != null) {
    		throw new PersonException("Email already taken!");
    	} else if (personService.getUsersByUsername(register.getUsername()) != null) {
    		throw new PersonException("Username already taken!");
    	}
    		
    	try {
    		personService.addUser(new Person(
            		register.getUsername(), 
            		register.getEmail(),
            		BCrypt.hashpw(register.getPassword(), System.getenv("SECRET_KEY"))));
    	} catch (Exception ex) {
    		throw new PersonException("Unknown error occured!");
    	}
    }

    @CrossOrigin
    @GetMapping(path = "/allUsers")
    public List<Person> allUsers(@RequestParam(value = "auth") String jwt){
        try {
            JWTRequest request = new JWTRequest(jwt);

            if(request.isValid() && request.isMod()){
                return personService.getAllUsers()
                        .stream()
                        .filter(person -> !person.isMod())
                        .collect(Collectors.toList());
            } else {
            	throw new UnauthorizedException("Not authorized for this content.");
            }
                
            
        } catch (JWTDecodeException e){
            throw new UnauthorizedException("Not authorized for this content.");
        }
    }

    @CrossOrigin
    @GetMapping("/{userid}")
    public Person personInfo(@PathVariable("userid") int userid){
        return personService.getUsersByID(userid);
    }

    @CrossOrigin
    @PostMapping("/delete/{userid}")
    public void deleteUser(@PathVariable("userid") int userid, @RequestParam("auth") String jwt){
        try {
            JWTRequest jwtRequest = new JWTRequest(jwt);
            if (jwtRequest.isMod()){
                Person person = personService.getUsersByID(userid);
                List<Recipe> recipes = person.getRecipes();
                recipes.stream().forEach( r -> recipeService.deleteRecipeById(r.getRecipeid()));
                personService.deleteUser(person);
            } else throw new UnauthorizedException("Only moderators can delete other users.");


        } catch (JWTDecodeException e){
        	throw new UnauthorizedException("Cannot do this action.");
        }

    }
}
