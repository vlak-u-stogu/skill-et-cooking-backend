package vus.sec.main.controller;

import com.auth0.jwt.exceptions.JWTDecodeException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import vus.sec.main.communication.JWTRequest;
import vus.sec.main.communication.UnauthorizedException;
import vus.sec.main.communication.recipes.RecipeForm;
import vus.sec.main.person.Person;
import vus.sec.main.person.PersonService;
import vus.sec.main.recipe.Recipe;
import vus.sec.main.recipe.RecipeException;
import vus.sec.main.recipe.RecipeService;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

@RestController
@RequestMapping("/recipes")
public class RecipeController {

    @Autowired
    private RecipeService recipeService;
    @Autowired
    private PersonService personService;

    @CrossOrigin
    @RequestMapping(path = "/page/{page}/", method = RequestMethod.GET)
    public List<Recipe> getRecipes(@PathVariable(value = "page") int page,
                                      @RequestParam(value = "num") int num,
                                      @RequestParam(value = "sort", required = false) String sort,
                                      @RequestParam(value = "search", required = false) String search){
        if(search == null)
            return recipeService.getAllRecipes(page, num, sort);
        else
            return recipeService.getRecipesByName(page, num, sort, search);
    }
    
    @CrossOrigin
    @RequestMapping(path = "/page/{page}/", method = RequestMethod.POST)
    public List<Recipe> getRecipesByIngredients(@PathVariable(value = "page") int page,
                                      @RequestParam(value = "num") int num,
                                      @RequestBody List<String> ingrNames,
                                      @RequestParam(value = "search", required = false) String search){
    	
            return recipeService.getRecipesByIngredients(page, num, ingrNames, search, 0.3);

    }
    @CrossOrigin
    @GetMapping(path = "/{recipeid}")
    public void increaseView(@PathVariable("recipeid") int recipeid) throws RecipeException {
        Recipe recipe = recipeService.getRecipeById(recipeid);
        recipe.setViews(recipe.getViews() + 1);
        recipeService.addRecipe(recipe);
    }

    @CrossOrigin
    @GetMapping(params = "auth")
    public List<Recipe> getRecipesJWT(@RequestParam("auth") String jwt){
        try {
            JWTRequest jwtRequest = new JWTRequest(jwt);
            if (jwtRequest.isValid() & !jwtRequest.isMod()){ 	
                return recipeService.getRecipesByPersonUsername(jwtRequest.getUsername());
            } else throw new UnauthorizedException("Unauthorized for this content.");
        } catch (JWTDecodeException e){
            throw new UnauthorizedException("Unauthorized for this content.");
        }
    }

    @CrossOrigin
    @GetMapping(params = "userid")
    public List<Recipe> getRecipesPId(@RequestParam("userid") int userid){
        return recipeService.getRecipesByPId(userid);
    }

    @CrossOrigin
    @PostMapping(path = "/{recipeid}/review")
    public void newReview(@PathVariable("recipeid") int recipeid,
                          @RequestParam("num") int reviewvalue,
                          @RequestParam("auth") String jwt) throws Exception {
        try {
            JWTRequest jwtRequest = new JWTRequest(jwt);
            Person person = personService.getUsersByUsername(jwtRequest.getUsername());
            if(reviewvalue <= 0 || reviewvalue >= 6) throw new RecipeException("Review value must be between 1 and 5.");
            recipeService.addReview(reviewvalue, person.getUserid(), recipeid);

        } catch (JWTDecodeException e) {
            throw new UnauthorizedException("Not allowed to do this.");
        }
          catch (RecipeException e){
            throw new RecipeException(e.getMessage());
          }
    }

    @CrossOrigin
    @RequestMapping(path = "/add", method  = RequestMethod.POST)
    public void newRecipe(@Valid @RequestPart("recipe") RecipeForm recipeForm, Errors errors,
    					  @RequestPart("0") Optional<MultipartFile> file0,
    					  @RequestPart("1") Optional<MultipartFile> file1,
    					  @RequestPart("2") Optional<MultipartFile> file2,
    					  @RequestPart("3") Optional<MultipartFile> file3,
    					  @RequestPart("4") Optional<MultipartFile> file4,
                          @RequestParam("auth") String jwt) throws RecipeException{
        try{
            // authorize user and validate data
            JWTRequest jwtRequest = new JWTRequest(jwt);
            if(jwtRequest.isValid() && !jwtRequest.isMod()) {
                if (errors.hasErrors()) {
                    AtomicReference<String> errMsg = new AtomicReference<>("");
                    errors.getAllErrors()
                            .stream()
                            .forEach(objectError -> errMsg.set(errMsg + objectError.getDefaultMessage() + " "));
                    throw new RecipeException(errMsg.get());
                }
            // add new recipe to database
                Person person = personService.getUsersByUsername(jwtRequest.getUsername());
                Recipe recipe = new Recipe(recipeForm.getName(), recipeForm.getDuration(), person.getUserid());
                recipeService.addRecipe(recipe);
                System.out.println(recipeForm.getIngredients());
                recipeService.addIngredients(recipeForm.getIngredients(), recipe.getRecipeid());
                recipeService.addSteps(recipeForm.getSteps(), recipe.getRecipeid());


                if (file0.isPresent()) recipeService.addPicture(0, file0.get(), recipe.getRecipeid());
                if (file1.isPresent()) recipeService.addPicture(1, file1.get(), recipe.getRecipeid());
                if (file2.isPresent()) recipeService.addPicture(2, file2.get(), recipe.getRecipeid());
                if (file3.isPresent()) recipeService.addPicture(3, file3.get(), recipe.getRecipeid());
                if (file4.isPresent()) recipeService.addPicture(4, file4.get(), recipe.getRecipeid());
                	
            } else
                throw new UnauthorizedException("Not authorized to do this.");

        } catch (JWTDecodeException e){
            throw new UnauthorizedException("Not authorized to do this.");
        }
          catch (RecipeException e){
            throw new RecipeException(e.getMessage());
        }
          
    }

    @CrossOrigin
    @PostMapping(path = "/update")
    public void updateRecipe(@Valid @RequestPart("recipe") RecipeForm recipeForm, Errors errors,
                          @RequestParam("id") int recipeid,
                          @RequestParam("auth") String jwt,
                          @RequestParam(value="update", required=false) List<Integer> update,
                          @RequestPart("0") Optional<MultipartFile> file0,
    					  @RequestPart("1") Optional<MultipartFile> file1,
    					  @RequestPart("2") Optional<MultipartFile> file2,
    					  @RequestPart("3") Optional<MultipartFile> file3,
    					  @RequestPart("4") Optional<MultipartFile> file4) throws RecipeException{
        try{
            // authorize user and validate data
            JWTRequest jwtRequest = new JWTRequest(jwt);
            if(jwtRequest.isValid() && !jwtRequest.isMod()) {
                if (errors.hasErrors()) {
                    AtomicReference<String> errMsg = new AtomicReference<>("");
                    errors.getAllErrors()
                            .stream()
                            .forEach(objectError -> errMsg.set(errMsg + objectError.getDefaultMessage() + " "));
                    throw new RecipeException(errMsg.get());
                }
            // get existing recipe
                Recipe recipe = recipeService.getRecipeById(recipeid);
            // check if it is the owner of recipe
                if(!recipe.getPerson().getUsername().equals(jwtRequest.getUsername()))
                    throw new RecipeException("You cannot do this action.");
            // delete excess data
                recipeService.deleteIngredients(recipe.getIngredients2(), recipeid);
                recipeService.deleteSteps(recipe.getSteps());
            // set new information
                recipe.setName(recipeForm.getName());
                recipe.setDuration(recipeForm.getDuration());
                recipeService.addIngredients(recipeForm.getIngredients(), recipeid);
                recipeService.addSteps(recipeForm.getSteps(), recipeid);

                // add images
                if (update != null) {
                	List<Optional<MultipartFile>> files = new ArrayList<>();
                    files.add(file0);
                    files.add(file1);
                    files.add(file2);
                    files.add(file3);
                    files.add(file4);
                    
                    for (int i = 0; i < files.size(); i++) {
                    	if (update.contains(i)) {
                    		if (files.get(i).isPresent()) {
                    			recipeService.addPicture(i, files.get(i).get(), recipe.getRecipeid());
                    		} else {
                    			recipeService.deletePicture(i, recipe.getRecipeid());
                    		}
                    	}
                    } 
                }
                
            } else
                throw new UnauthorizedException("Not authorized to do this.");

        } catch (JWTDecodeException e){
            throw new UnauthorizedException("Not authorized to do this.");
        }
        catch (RecipeException e){
            throw new RecipeException(e.getMessage());
        }
    }

    @CrossOrigin
    @RequestMapping(path = "/delete", method = RequestMethod.POST)
    public void deleteRecipe(@RequestParam int recipeid, @RequestParam(value = "auth") String jwt){
        try{
            JWTRequest jwtRequest = new JWTRequest(jwt);
            Recipe tmp = recipeService.getRecipeById(recipeid);
            if(jwtRequest.isValid() && tmp != null){
                if(tmp.getPerson().getUsername().equals(jwtRequest.getUsername()) || jwtRequest.isMod()){
                    recipeService.deleteRecipeById(tmp.getRecipeid());
                } else {
                    throw new UnauthorizedException("Unauthorized");
                }
            }
        } catch (Exception exc){
            throw new UnauthorizedException("Unauthorized");
        }
    }

    
    
}
