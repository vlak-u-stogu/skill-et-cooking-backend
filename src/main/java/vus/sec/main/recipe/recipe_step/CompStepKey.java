package vus.sec.main.recipe.recipe_step;

import java.io.Serializable;
import java.util.Objects;

public class CompStepKey  implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int stepnum;
    private int recipeid;

    public CompStepKey() {
    }

    public CompStepKey(int stepnum, int recipeid) {
        this.stepnum = stepnum;
        this.recipeid = recipeid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CompStepKey that = (CompStepKey) o;
        return stepnum == that.stepnum && recipeid == that.recipeid;
    }

    @Override
    public int hashCode() {
        return Objects.hash(stepnum, recipeid);
    }
}
