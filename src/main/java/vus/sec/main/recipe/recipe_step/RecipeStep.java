package vus.sec.main.recipe.recipe_step;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "recipe_step")
@IdClass(CompStepKey.class)
public class RecipeStep implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
    private int stepnum;
    private String description;
    @Id
    private int recipeid;


    public RecipeStep(int stepnum, String description, int recipeid) {
        this.stepnum = stepnum;
        this.description = description;
        this.recipeid = recipeid;
        //this.compKeyStep = new CompKeyStep(step_num, recipeID);
    }

    public RecipeStep() {
    }

    public int getStepnum() {
        return stepnum;
    }

    public void setStepnum(int step_num) {
        this.stepnum = step_num;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getRecipeID() {
        return recipeid;
    }

    public void setRecipeID(int recipeID) {
        this.recipeid = recipeID;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RecipeStep that = (RecipeStep) o;
        return stepnum == that.stepnum && recipeid == that.recipeid && description.equals(that.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(stepnum, description, recipeid);
    }

    @Override
    public String toString() {
        return "RecipeStep{" +
                "step_num=" + stepnum +
                ", description='" + description + '\'' +
                ", recipeID=" + recipeid +
                '}';
    }
}
