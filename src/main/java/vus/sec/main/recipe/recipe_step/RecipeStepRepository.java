package vus.sec.main.recipe.recipe_step;

import org.springframework.data.jpa.repository.JpaRepository;

public interface RecipeStepRepository extends JpaRepository<RecipeStep, CompStepKey> {
}
