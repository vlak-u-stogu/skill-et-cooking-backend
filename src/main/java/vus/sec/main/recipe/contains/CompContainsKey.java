package vus.sec.main.recipe.contains;

import java.io.Serializable;
import java.util.Objects;

public class CompContainsKey implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int recipeid;
    private int ingredientid;

    public CompContainsKey() {
    }

    public CompContainsKey(int recipeid, int ingredientid) {
        this.recipeid = recipeid;
        this.ingredientid = ingredientid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CompContainsKey that = (CompContainsKey) o;
        return recipeid == that.recipeid && ingredientid == that.ingredientid;
    }

    @Override
    public int hashCode() {
        return Objects.hash(recipeid, ingredientid);
    }
}
