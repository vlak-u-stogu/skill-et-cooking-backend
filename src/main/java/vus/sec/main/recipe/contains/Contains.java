package vus.sec.main.recipe.contains;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

import vus.sec.main.ingredient.Ingredient;
import vus.sec.main.recipe.Recipe;

import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "containsIngredient")
@IdClass(CompContainsKey.class)
public class Contains implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String amount;
	
    @Id
    private int recipeid;
    @Id
    private int ingredientid;

    @ManyToOne
    @MapsId("recipeid")
    @JoinColumn(name = "recipeid")
    private Recipe recipe;
    
    @ManyToOne
    @MapsId("ingredientid")
    @JoinColumn(name = "ingredientid")
    private Ingredient ingredient;

    public Contains() {
    }

    public Contains(String amount, int recipeid, int ingredientid) {
        this.amount = amount;
        this.recipeid = recipeid;
        this.ingredientid = ingredientid;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

//    public int getRecipeid() {
//        return recipeid;
//    }

    public void setRecipeid(int recipeid) {
        this.recipeid = recipeid;
    }

    public int getIngredientid() {
        return ingredientid;
    }

    public void setIngredientid(int ingredientid) {
        this.ingredientid = ingredientid;
    }
    
	public String getIngredientName() {
		return ingredient.getName();
	}

	public void setIngredient(Ingredient ingredient) {
		this.ingredient = ingredient;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Contains contains = (Contains) o;
        return amount == contains.amount && recipeid == contains.recipeid && ingredientid == contains.ingredientid;
    }

    @Override
    public int hashCode() {
        return Objects.hash(amount, recipeid, ingredientid);
    }

    @Override
    public String toString() {
        return "Contains{" +
                "amount=" + amount +
                ", recipeid=" + recipeid +
                ", ingredientid=" + ingredientid +
                '}';
    }
}
