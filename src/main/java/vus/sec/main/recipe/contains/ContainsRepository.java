package vus.sec.main.recipe.contains;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ContainsRepository extends JpaRepository<Contains, CompContainsKey> {
}
