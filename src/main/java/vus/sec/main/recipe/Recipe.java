package vus.sec.main.recipe;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import vus.sec.main.ingredient.Ingredient;
import vus.sec.main.person.Person;
import vus.sec.main.recipe.contains.Contains;
import vus.sec.main.recipe.recipe_picture.RecipePicture;
import vus.sec.main.recipe.recipe_step.RecipeStep;
import vus.sec.main.recipe.reviewed.Reviewed;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "recipe")
public class Recipe {
    @Id
    @GeneratedValue(generator = "recipe_recipeid_seq")
    private int recipeid;
    private String name;
    private int views;
    private int duration;
    private int userid;

    @ManyToMany(fetch = FetchType.EAGER)
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @JoinTable(name = "containsIngredient",
            joinColumns = @JoinColumn(name = "recipeid"),
            inverseJoinColumns = @JoinColumn(name = "ingredientid"))
    private List<Ingredient> ingredientsOld;
    
    @OneToMany(mappedBy = "recipe")
    private List<Contains> ingredients;

	@OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "recipeid")
    private List<RecipeStep> steps;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "recipeid")
    private List<RecipePicture> pictures;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "userid",
            insertable = false,
            updatable = false,
            nullable = false)
    @JsonIgnoreProperties("recipes")
    private Person person;

    @OneToMany
    @JoinColumn(name = "recipeid")
    private List<Reviewed> reviews;

    public Recipe(String name, int duration, int userid){
        this.name = name;
        this.views = 0;
        this.duration = duration;
        this.userid = userid;
    }
    public Recipe(){
    }
    public int getRecipeid() {
        return recipeid;
    }

    public void setRecipeid(int recipeid) {
        this.recipeid = recipeid;
    }

    public String getName() {
        return name;
    }

    public void setName(String recipeName) {
        this.name = recipeName;
    }

    public int getViews() {
        return views;
    }

    public void setViews(int views) {
        this.views = views;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public int getUserid(){ return userid; }

    public void setUserid(int userid){ this.userid = userid; }

    public List<Ingredient> getIngredients2() {
        return ingredientsOld;
    }

    public void setIngredientsOld(List<Ingredient> ingredients) {
        this.ingredientsOld = ingredients;
    }

    public List<RecipeStep> getSteps() {
        return steps;
    }

    public void setSteps(List<RecipeStep> step) {
        this.steps = step;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public List<Reviewed> getReviews() {
        return reviews;
    }

    public void setReviews(List<Reviewed> reviews) {
        this.reviews = reviews;
    }

    public List<RecipePicture> getPictures() {
        return pictures;
    }

    public void setPictures(List<RecipePicture> pictures) {
        this.pictures = pictures;
    }
    
    public List<Contains> getIngredients() {
		return ingredients;
	}
	public void setIngredients(List<Contains> ingredients) {
		this.ingredients = ingredients;
	}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Recipe recipe = (Recipe) o;
        return recipeid == recipe.recipeid && views == recipe.views && duration == recipe.duration && userid == recipe.userid && name.equals(recipe.name) && steps.equals(recipe.steps) && person.equals(recipe.person);
    }

    @Override
    public int hashCode() {
        return Objects.hash(recipeid, name, views, duration, userid, steps, person);
    }

    @Override
    public String toString() {
        return "Recipe{" +
                "recipeid=" + recipeid +
                ", name='" + name + '\'' +
                ", views=" + views +
                ", duration=" + duration +
                ", userid=" + userid +
                '}';
    }

    public Double getAverageReview() {
        OptionalDouble avg = this.getReviews().stream().mapToDouble(r -> r.getReviewvalue()).average();
        if(avg.isPresent()) return avg.getAsDouble();
        else
            return 0.0;
    }
}
