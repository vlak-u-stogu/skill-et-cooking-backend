package vus.sec.main.recipe;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import vus.sec.main.communication.recipes.AmountAndName;
import vus.sec.main.ingredient.Ingredient;
import vus.sec.main.ingredient.IngredientRepository;
import vus.sec.main.person.Person;
import vus.sec.main.person.PersonRepository;
import vus.sec.main.recipe.contains.CompContainsKey;
import vus.sec.main.recipe.contains.Contains;
import vus.sec.main.recipe.contains.ContainsRepository;
import vus.sec.main.recipe.recipe_picture.CompPictureKey;
import vus.sec.main.recipe.recipe_picture.PictureUploadService;
import vus.sec.main.recipe.recipe_picture.RecipePicture;
import vus.sec.main.recipe.recipe_picture.RecipePictureRepository;
import vus.sec.main.recipe.recipe_step.RecipeStep;
import vus.sec.main.recipe.recipe_step.RecipeStepRepository;
import vus.sec.main.recipe.reviewed.Reviewed;
import vus.sec.main.recipe.reviewed.ReviewedRepository;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class RecipeService {
    @Autowired
    private RecipeRepository recipeRepository;
    @Autowired
    private RecipeStepRepository recipeStepRepository;
    @Autowired
    private RecipePictureRepository recipePictureRepository;
    @Autowired
    private ContainsRepository containsRepository;
    @Autowired
    private ReviewedRepository reviewedRepository;
    @Autowired
    private PersonRepository personRepository;
    @Autowired
    private IngredientRepository ingredientRepository;
 
    @Autowired
    private PictureUploadService pictureUploadService;


    public List<Recipe> getAllRecipes(int page, int num, String sort){
        Comparator<Recipe> function = getSort(sort);
        return getRecipes(page, num, recipeRepository.findAll().stream()
                .sorted(function).collect(Collectors.toList()));
    }

    public List<Recipe> getRecipesByName(int page, int num, String sort, String search){
        Comparator<Recipe> function = getSort(sort);
        List<Recipe> filteredRecipes = recipeRepository.findAll().stream()
                                    .filter( recipe -> recipe.getName().toLowerCase().contains(search.toLowerCase()))
                                    .sorted(function)
                                    .collect(Collectors.toList());
        return getRecipes(page, num, filteredRecipes);
    }

    public List<Recipe> getRecipesByIngredients(int page, int num, List<String> names, String search, double threshold){
        List<Ingredient> ingredients = new ArrayList<>();
        for(int i = 0; i < names.size(); ++i)
            ingredients.add(ingredientRepository.getIngredientByName(names.get(i)));

        List<Recipe> filteredRecipes;

        Comparator<List<Ingredient>> comparator = new IngredientComparator(ingredients);
        if(search == null || search.equals(""))
        filteredRecipes = recipeRepository.findAll().stream()
                .sorted((a, b) -> comparator.compare(a.getIngredients2(), b.getIngredients2()))
                .filter(r -> jaccardSim(r.getIngredients2(), ingredients) > threshold)
                .collect(Collectors.toList());
        else filteredRecipes = recipeRepository.findAll().stream()
                .filter(r -> r.getName().toLowerCase().contains(search.toLowerCase()))
                .sorted((a, b) -> comparator.compare(a.getIngredients2(), b.getIngredients2()))
                .filter(r -> jaccardSim(r.getIngredients2(), ingredients) > threshold)
                .collect(Collectors.toList());
        return getRecipes(page, num, filteredRecipes);
    }

    public Recipe getRecipeById(int recipeid){
        return recipeRepository.findById(recipeid).get();
    }

    public List<Recipe> getRecipesByPersonUsername(String username){
        Person person = personRepository.getPersonByUsername(username);
        return person.getRecipes();
    }

    public List<Recipe> getRecipesByPId(int userid){
        Optional<Person> person = personRepository.findById(userid);
        return person.isPresent() ? person.get().getRecipes() : new ArrayList<>();
    }

    public void addRecipe(Recipe r) throws RecipeException{
        try{
            recipeRepository.save(r);
        } catch(Exception exc){

        }
    }

    public void deleteRecipeById(int recipeId) {
        Recipe recipe = recipeRepository.findById(recipeId).get();
        recipe.getSteps().stream().forEach(recipeStep -> recipeStepRepository.delete(recipeStep));
        recipe.getPictures().stream().forEach(recipePicture -> recipePictureRepository.delete(recipePicture));
        recipe.getReviews().stream().forEach(reviewed -> reviewedRepository.delete(reviewed));
        recipeRepository.deleteById(recipeId);
    }

    public void addIngredients(List<AmountAndName> amountAndName, int recipeid){

        for(int i = 0; i < amountAndName.size(); ++i){
            Ingredient ingr = ingredientRepository.getIngredientByName(amountAndName.get(i).getName());
            if (ingr == null) {
                ingr = new Ingredient(amountAndName.get(i).getName());
                ingredientRepository.save(ingr);
            }
            Contains contains = new Contains(amountAndName.get(i).getAmount(), recipeid, ingr.getIngredientID());
            containsRepository.save(contains);
        }



    }

    public void deleteIngredients(List<Ingredient> ingredients, int recipeid){
        List<CompContainsKey> keys = new LinkedList<>();
        ingredients.stream()
                .forEach(i -> {
                    keys.add(new CompContainsKey(recipeid, i.getIngredientID()));
                });
        containsRepository.deleteAllById(keys);
    }
    public void deleteSteps(List<RecipeStep> recipeSteps){
        recipeStepRepository.deleteAll(recipeSteps);
    }
    public void deletePictures(List<RecipePicture> recipePictures){
        recipePictureRepository.deleteAll(recipePictures);
    }

    public void addSteps(List<String> description, int recipeid){
        for(int i = 0; i < description.size(); ++i){
            RecipeStep step = new RecipeStep(i + 1, description.get(i), recipeid);
            recipeStepRepository.save(step);
        }

    }

    public void addPicture(int picnum, MultipartFile pictureFile, int recipeid){	
    	String picfilelocation = pictureUploadService.upload(pictureFile);
        RecipePicture picture = new RecipePicture(picnum, picfilelocation, recipeid);
        recipePictureRepository.saveAndFlush(picture);
    }
    
    public void deletePicture(int picnum, int recipeid) {
    	try {
    		recipePictureRepository.delete(recipePictureRepository.getById(new CompPictureKey(picnum, recipeid)));
    	} catch (Exception ex) {
    		
    	}
    	
    }

    public void addReview(int reviewvalue, int userid, int recipeid){
        Reviewed reviewed = new Reviewed(reviewvalue, userid, recipeid);
        reviewedRepository.save(reviewed);
    }

    private List<Recipe> getRecipes(int page, int num, List<Recipe> recipes) {
        if( page * num <= recipes.size())
            return recipes.subList((page - 1) * num , page * num);
        else if( page * num > recipes.size() && (page - 1)*num < recipes.size())
            return recipes.subList((page - 1)*num, recipes.size());
        else
            return null;
    }
    private Comparator<Recipe> getSort(String sort){
        Comparator<Recipe> function = (a, b) -> b.getViews() - a.getViews();
        if(sort == null) return function;
        if(sort.equals("topRated")) {
            function = (a, b) -> b.getAverageReview().compareTo(a.getAverageReview());
        }
        else if(sort.equals("recommended")) {
            function = (a, b) -> {
                Integer v1 = a.getViews();
                Integer v2 = b.getViews();
                Double o1 = a.getAverageReview();
                Double o2 = b.getAverageReview();
                Double r1 = (v1 / (v1 + v2 + 1)) + (o1 / (o1 + o2 + 1));
                Double r2 = (v2 / (v1 + v2 + 1)) + (o2 / (o1 + o2 + 1));
                return r2.compareTo(r1);
            };
        }
        return function;
    }

    public static double jaccardSim(List<Ingredient> ingredients, List<Ingredient> searched){
        double jacc1;
        HashSet<Ingredient> union1 = new HashSet<>(ingredients);
        union1.addAll(searched);
        HashSet<Ingredient> intersection1 = new HashSet<>(searched);
        intersection1.retainAll(new HashSet<>(ingredients));

        jacc1 = (double) intersection1.size() / union1.size();
        System.out.println(jacc1);
        return jacc1;
    }

    private static class IngredientComparator implements Comparator<List<Ingredient>> {
        private List<Ingredient> ingredients;
        private IngredientComparator(List<Ingredient> ingredients){
            this.ingredients = ingredients;
        }

        @Override
        public int compare(List<Ingredient> o1, List<Ingredient> o2) {
            double jacc1 = jaccardSim(o1, this.ingredients);
            double jacc2 = jaccardSim(o2, this.ingredients);

            if(jacc1 > jacc2) return -1;
                else
            if( jacc1 < jacc2) return 1;
                else
            return 0;
        }

    }
}
