package vus.sec.main.recipe;

import java.io.IOException;

public class RecipeException extends IOException {

	private static final long serialVersionUID = 1L;

	public RecipeException(String message){
        super(message);
    }

    public RecipeException(){
        super();
    }

}

