package vus.sec.main.recipe.recipe_picture;

import org.springframework.data.jpa.repository.JpaRepository;

public interface RecipePictureRepository extends JpaRepository<RecipePicture, CompPictureKey> {
}
