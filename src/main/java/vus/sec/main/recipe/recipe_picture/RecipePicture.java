package vus.sec.main.recipe.recipe_picture;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "recipe_picture")
@IdClass(CompPictureKey.class)
public class RecipePicture implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
    private int picnum;
    private String picfilelocation;
    @Id
    private int recipeid;

    public RecipePicture() {
    }

    public RecipePicture(int picnum, String picfilelocation, int recipeid) {
        this.picnum = picnum;
        this.picfilelocation = picfilelocation;
        this.recipeid = recipeid;
    }

    public int getPicnum() {
        return picnum;
    }

    public void setPicnum(int picnum) {
        this.picnum = picnum;
    }

    public String getPicfilelocation() {
        return picfilelocation;
    }

    public void setPicfilelocation(String picfilelocation) {
        this.picfilelocation = picfilelocation;
    }

    public int getRecipeid() {
        return recipeid;
    }

    public void setRecipeid(int recipeid) {
        this.recipeid = recipeid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RecipePicture that = (RecipePicture) o;
        return picnum == that.picnum && recipeid == that.recipeid && picfilelocation.equals(that.picfilelocation);
    }

    @Override
    public int hashCode() {
        return Objects.hash(picnum, picfilelocation, recipeid);
    }

    @Override
    public String toString() {
        return "RecipePicture{" +
                "picnum=" + picnum +
                ", picfilelocation='" + picfilelocation + '\'' +
                ", recipeid=" + recipeid +
                '}';
    }
}
