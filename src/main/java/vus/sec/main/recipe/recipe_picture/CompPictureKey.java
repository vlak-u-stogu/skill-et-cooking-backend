package vus.sec.main.recipe.recipe_picture;

import java.io.Serializable;
import java.util.Objects;

public class CompPictureKey implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int picnum;
    private int recipeid;

    public CompPictureKey() {
    }

    public CompPictureKey(int picnum, int recipeid) {
        this.picnum = picnum;
        this.recipeid = recipeid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CompPictureKey that = (CompPictureKey) o;
        return picnum == that.picnum && recipeid == that.recipeid;
    }

    @Override
    public int hashCode() {
        return Objects.hash(picnum, recipeid);
    }
}
