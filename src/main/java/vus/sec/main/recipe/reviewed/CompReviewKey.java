package vus.sec.main.recipe.reviewed;

import java.io.Serializable;
import java.util.Objects;

public class CompReviewKey implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int userid;
    private int recipeid;

    public CompReviewKey() {
    }

    public CompReviewKey(int userid, int recipeid) {
        this.userid = userid;
        this.recipeid = recipeid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CompReviewKey that = (CompReviewKey) o;
        return userid == that.userid && recipeid == that.recipeid;
    }

    @Override
    public int hashCode() {
        return Objects.hash(userid, recipeid);
    }
}
