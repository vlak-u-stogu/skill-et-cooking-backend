package vus.sec.main.recipe.reviewed;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "reviewed")
@IdClass(CompReviewKey.class)
public class Reviewed implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
    private int reviewvalue;
    private int userid;
    @Id
    private int recipeid;

    public Reviewed() {
    }

    public Reviewed(int reviewvalue, int userid, int recipeid) {
        this.reviewvalue = reviewvalue;
        this.userid = userid;
        this.recipeid = recipeid;
    }

    public int getReviewvalue() {
        return reviewvalue;
    }

    public void setReviewvalue(int reviewvalue) {
        this.reviewvalue = reviewvalue;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public int getRecipeid() {
        return recipeid;
    }

    public void setRecipeid(int recipeid) {
        this.recipeid = recipeid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Reviewed reviewed = (Reviewed) o;
        return reviewvalue == reviewed.reviewvalue && userid == reviewed.userid && recipeid == reviewed.recipeid;
    }

    @Override
    public int hashCode() {
        return Objects.hash(reviewvalue, userid, recipeid);
    }

    @Override
    public String toString() {
        return "Reviewed{" +
                "reviewvalue=" + reviewvalue +
                ", userid=" + userid +
                ", recipeid=" + recipeid +
                '}';
    }
}
