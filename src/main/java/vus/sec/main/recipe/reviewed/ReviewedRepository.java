package vus.sec.main.recipe.reviewed;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ReviewedRepository extends JpaRepository<Reviewed, CompReviewKey> {
}
