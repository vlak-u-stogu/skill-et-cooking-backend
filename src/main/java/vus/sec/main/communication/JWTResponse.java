package vus.sec.main.communication;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.LinkedHashMap;
import java.util.Map;

public class JWTResponse {
    private String jwt;
    private String username;
    private int isModerator;
    private final LocalDateTime expDate = LocalDateTime.now().plusHours(8);

    public JWTResponse(String username, int isModerator) {
        this.username = username;
        this.isModerator = isModerator;
        Map<String, Object> header = new LinkedHashMap<>();
        header.put("alg", "HS256");
        header.put("typ", "JWT");
        String jwtWithMySignature = JWT.create()
                .withExpiresAt(java.util.Date
                        .from(this.expDate.atZone(ZoneId.systemDefault())
                                .toInstant()))
                .withHeader(header)
                .withClaim("username", this.username)
                .withClaim("isModerator", this.isModerator)
                .sign(Algorithm.HMAC256(System.getenv("SECRET_KEY")));
        this.jwt = jwtWithMySignature;
    }
    public JWTResponse(){
    };

    public String getJwt() {
        return jwt;
    }

    public void setJwt(String jwt) {
        this.jwt = jwt;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getIsModerator() {
        return isModerator;
    }

    public void setIsModerator(int isModerator) {
        this.isModerator = isModerator;
    }

    public LocalDateTime getExpDate() {
        return expDate;
    }
}
