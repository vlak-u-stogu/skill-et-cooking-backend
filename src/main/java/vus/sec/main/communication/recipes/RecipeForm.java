package vus.sec.main.communication.recipes;

import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.util.List;

@Valid
public class RecipeForm {
    @NotBlank(message = "Recipe must have a name.")
    private String name;
    @NotNull(message = "Duration is mandatory.")
    @Positive(message = "Duration must be greater than 0.")
    private Integer duration;
    @Size(min = 1, message = "Recipe must have at least 1 ingredient.")
    private List<AmountAndName> ingredients;
    @Size(min = 1, message = "Recipe must have at least 1 step.")
    private List<String> steps;

    public RecipeForm() {
    }

    public RecipeForm(String name, int duration,
                      List<AmountAndName> ingredients,
                      List<String> steps,
                      List<MultipartFile> pictures) {
        this.name = name;
        this.duration = duration;
        this.ingredients = ingredients;
        this.steps = steps;
    }

    public RecipeForm(String name,
                      int duration,
                      List<AmountAndName> ingredients,
                      List<String> steps) {
        this.name = name;
        this.duration = duration;
        this.ingredients = ingredients;
        this.steps = steps;
    }

    public String getName() {
        return name;
    }

    public int getDuration() {
        return duration;
    }

    public List<AmountAndName> getIngredients() {
        return ingredients;
    }

    public List<String> getSteps() {
        return steps;
    }
}
