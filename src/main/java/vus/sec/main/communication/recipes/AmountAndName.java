package vus.sec.main.communication.recipes;

public class AmountAndName {
    private String name;
    private String amount;

    public AmountAndName() {
    }

    public AmountAndName(String name, String amount) {
        this.name = name;
        this.amount = amount;
    }

    public String getName() {
        return name;
    }

    public String getAmount() {
        return amount;
    }

    @Override
    public String toString() {
        return "AmountAndName{" +
                "name=" + name +
                ", amount='" + amount + '\'' +
                '}';
    }
}
