package vus.sec.main.communication;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class RegisterForm {

	@NotBlank(message = "Username is mandatory.")
	@Size(min = 5, max = 30, message = "Username must be between 5 and 30 characters.")
	private String username;
	
	@NotBlank(message = "Email is mandatory.")
    @Email(message = "Email is not in the correct format.")
	private String email;
	
	@NotBlank(message = "Password is mandatory.")
	@Size(min = 8, message = "Password must have at least 8 characters.")
	private String password;
	
	public RegisterForm(String username, String email, String password) {
		super();
		this.username = username;
		this.email = email.toLowerCase();
		this.password = password;
	}
	public String getUsername() {
		return username;
	}
	public String getEmail() {
		return email;
	}
	public String getPassword() {
		return password;
	}
	
	

}
