package vus.sec.main.communication;

import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.JWT;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.LinkedHashMap;
import java.util.Map;

public class JWTRequest {
    private String jwt;
    private String username;
    private int isModerator;
    private LocalDateTime expDate;

    public JWTRequest(String jwt) {
        this.jwt = jwt;
        if( this.jwt != null) {
            Map<String, Claim> data = JWT.decode(this.jwt).getClaims();
            this.expDate = LocalDateTime.ofInstant(
                    JWT.decode(this.jwt).getExpiresAt().toInstant(), ZoneId.systemDefault());
            this.username = data.get("username").asString();
            this.isModerator = data.get("isModerator").asInt();
        }

    }

    public JWTRequest() {
    }

    public String getJwt() {
        return jwt;
    }

    public void setJwt(String jwt) {
        this.jwt = jwt;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getIsModerator() {
        return isModerator;
    }

    public void setIsModerator(int isModerator) {
        this.isModerator = isModerator;
    }

    public LocalDateTime getExpDate() {
        return expDate;
    }

    public void setExpDate(LocalDateTime expDate) {
        this.expDate = expDate;
    }

    public boolean isValid(){
        if(this.jwt == null && LocalDateTime.now().isAfter(expDate)) return false;
        Map<String, Object> header = new LinkedHashMap<>();
        header.put("alg", "HS256");
        header.put("typ", "JWT");
        String jwtWithMySignature = JWT.create()
                .withExpiresAt(java.util.Date
                        .from(expDate.atZone(ZoneId.systemDefault())
                                .toInstant()))
                .withHeader(header)
                .withClaim("username", this.username)
                .withClaim("isModerator", this.isModerator)
                .sign(Algorithm.HMAC256(System.getenv("SECRET_KEY")));
        return jwtWithMySignature.equals(this.jwt);
    }
    public boolean isMod(){
        return this.isModerator == 1;
    }
}
