package vus.sec.main.communication;

public class LoginForm {
    private String usernameORemail;
    private String password;

    public LoginForm(String usernameORemail, String password) {
        this.usernameORemail = usernameORemail;
        this.password = password;
    }

    public String getUsernameORemail() {
        return usernameORemail;
    }

    public String getPassword() {
        return password;
    }
}
