package vus.sec.main.person;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PersonService {

    @Autowired
    private PersonRepository personRepository;

    public List<Person> getAllUsers(){
        return personRepository.findAll();
    }
    public Person getUsersByUsernameAndEmail(String username, String email){
        return personRepository.getPersonByUsernameAndEmail(username, email);
    }
    public Person getUsersByUsername(String username){
        return personRepository.getPersonByUsername(username);
    }
    public Person getUsersByEmail(String email){
        return personRepository.getPersonByEmail(email);
    }
    public Person getUsersByID(int id) {
        return personRepository.findById(id).get();
    }
    public void addUser(Person p){
        personRepository.save(p);
    }
    public void deleteUser(Person p){
        personRepository.delete(p);
    }
}
