package vus.sec.main.person;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.mindrot.jbcrypt.BCrypt;
import vus.sec.main.recipe.Recipe;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Table
public class Person {
	
    @Id
    @GeneratedValue(generator = "person_userid_seq")
    private int userid;
    private String username;
    private String email;
    private String hashedPassword;

    private int isModerator;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "userid")
    @JsonIgnoreProperties("person")
    private List<Recipe> recipes;

    public Person(String username, String email, String hashedPassword) {
        this.username = username;
        this.email = email;
        this.hashedPassword = hashedPassword;
        this.isModerator = 0;
    }

    public Person() {

    }
    public Integer getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setHashedPassword(String hashedPassword) {
        this.hashedPassword = hashedPassword;
    }

    public int getIsModerator() {
        return isModerator;
    }

    public void setIsModerator(int isModerator) {
        this.isModerator = isModerator;
    }

    public List<Recipe> getRecipes() {
        return recipes;
    }

    public void setRecipes(List<Recipe> recipes) {
        this.recipes = recipes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return userid == person.userid && isModerator == person.isModerator && username.equals(person.username) && email.equals(person.email) && hashedPassword.equals(person.hashedPassword) && Objects.equals(recipes, person.recipes);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userid, username, email, hashedPassword, isModerator, recipes);
    }

    @Override
    public String toString() {
        return "Person{" +
                "userid=" + userid +
                ", username='" + username + '\'' +
                ", email='" + email + '\'' +
                ", hashedPassword='" + hashedPassword + '\'' +
                ", isModerator=" + isModerator +
                '}';
    }

    public boolean isMod(){
        return this.isModerator == 1;
    }

    public boolean checkPassword(String password){
        return !BCrypt.hashpw(password, System.getenv("SECRET_KEY")).equals(this.hashedPassword);
    }
}
