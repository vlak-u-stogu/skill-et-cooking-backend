package vus.sec.main.person;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonRepository extends JpaRepository<Person, Integer> {
    Person getPersonByUsername(String username);
    Person getPersonByEmail(String email);
    Person getPersonByUsernameAndEmail(String username, String email);
}
